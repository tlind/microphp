# Usage

1) Clone the repo

2) Run bin/start.sh

# Micro Classes

The document root is the 'http' folder. In it, application.php catches all dynamic requests. In three lines it includes the MicroPHP class, instantiates it and lets it process the request to either send a 404 or route the request as necessary.

## MicroPHP

Has four properties, you can control what classes are instantiated for these:
	- server
	- request
	- response
	- router

## MicroServer

Extracts only the values from $_SERVER which are actually considered part of the 'server', and defines an 'application root' in addition to 'document root'.

## MicroRequest

Extracts the values from $_SERVER which are considered part of the request. Does some parsing of those values, which is necessary when doing custom routing.

## MicroResponse

Does output buffering which can help with the template / layout pattern.

## MicroRouter

This file defines a very useful global function `route`, check it out in the source.

# Skeleton

1) Portable Web Application Directory

Each time bin/start.sh is run, the server configuration is updated with the hardcoded path of wherever the application directory is,
which means that the application directory can be placed anywhere without you needing to hard code it's path into the server config.

2) PHP reflection of the absolute path of the application root directory.

PHP tells us what the document root is, but does not specify what the application root is, which may be used to contain log files / data files.
By default the application.php file will set the $_SERVER['APPLICATION_ROOT'] path to be the parent of the document root.

3) Default dynamic routing

If a file is not found, then the server will automatically rewrite the request to /application.php which provides the dynamic routes or 404 behavior.
/application.php should be used to do any booting up of the app, which should then be required at the top of all PHP files.

4) A server running on whatever hostnames you have pointing to your IP address (without any configuration, localhost).

In future you will be able to set the port and hostname used for each invocation of the application. The PHP app will automatically be able
to reflect on the port and hostname used by looking in $_SERVER['SERVER_PORT'] and $_SERVER['SERVER_NAME'], and the requested host with $_SERVER['HTTP_HOST'].

5) Include path setup

This is provided by PHP, the document root is always already on the include path, so put all your files and dependencies in there.




