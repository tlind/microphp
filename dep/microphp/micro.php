<?php

/*
 A highly decoupled "micro-framework" for rapid PHP application development.
*/

require_once 'server.php';
require_once 'request.php';
require_once 'response.php';
require_once 'router.php';

/* An application can subclass this, or just instantiate it. */
class MicroPHP {
	
	static $SERVER_CLASSNAME = "MicroServer";
	static $REQUEST_CLASSNAME = "MicroRequest";
	static $RESPONSE_CLASSNAME = "MicroResponse";
	static $ROUTER_CLASSNAME = "MicroRouter";
	
	function __construct() {
		
		$this->server = new MicroServer();
		$this->request = new MicroRequest();
		$this->response = new MicroResponse();

		$this->router = new MicroScriptRouter($this);
		
		// $this->states = new MicroStateManager();
		// $this->events = new MicroEventManager();
		// $this->data = array(); // new MicroDataManager();
		// $this->template = array(); // new MicroTemplate(); // store variables for a template (not data)
		// $this->forms = new MicroFormsManager();
		// $this->auth = new MicroAuth();
	}
	
	/*  */
	function process() {
		return $this->router->route($this);
	}
}

?>
