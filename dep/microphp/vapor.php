<?php

/* Extending the $_SERVER notion. 

// parse the request uri into it's parts
$_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$_SERVER['REQUEST_URI_QUERY'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);

// add application root folder, as well as controller and view folder
$_SERVER['APPLICATION_ROOT'] = dirname($_SERVER['DOCUMENT_ROOT']) . '/';
$_SERVER['CONTROLLER_ROOT'] = $_SERVER['APPLICATION_ROOT'] . 'lib/renderers';
$_SERVER['VIEW_ROOT'] = $_SERVER['DOCUMENT_ROOT'] . 'application/';

// add application, module and action name
$_SERVER['APPLICATION_NAME'] = null;
$_SERVER['MODULE_NAME'] = null;
$_SERVER['ACTION_NAME'] = null;

// the controller class and action on it can be determined by the module / action name or customized here.
$_SERVER['CONTROLLER_SCRIPT'] = null;
$_SERVER['CONTROLLER_CLASS'] = null;
$_SERVER['ACTION_METHOD'] = null;

// to help with routing
$_SERVER['ROUTER_SCRIPT'] = '/route.php';

// add a current_path to aid relative routing

if ($_SERVER['SCRIPT_NAME'] == $_SERVER['ROUTER_SCRIPT']) {
	$_SERVER['CURRENT_PATH'] = null;
	$_SERVER['REMAINING_PATH'] = $_SERVER['REQUEST_URI_PATH'];
}
else {
	$_SERVER['CURRENT_PATH'] = $_SERVER['REQUEST_URI_PATH'];
	$_SERVER['REMAINING_PATH'] = null;
}

*/

// Set the include path to include the directory that holds all of the control files. 

//set_include_path($_SERVER['CONTROLLER_ROOT'] . PATH_SEPARATOR . $_SERVER["DOCUMENT_ROOT"] . PATH_SEPARATOR . ($_SERVER['APPLICATION_ROOT'] . 'lib') . PATH_SEPARATOR . ($_SERVER['APPLICATION_ROOT'] . 'dep') . PATH_SEPARATOR . ini_get('include_path'));

?>
