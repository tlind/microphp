<?php

// Using a Server instance instead of $_SERVER lets us control this so that it works through command line as well.
class MicroServer {
	
	public function __construct($params = array()) {
		
		$this->ip = $_SERVER["SERVER_ADDR"];
			
		$this->port = $_SERVER["SERVER_PORT"];
			
		$this->host = $_SERVER["SERVER_NAME"];
			
		$this->protocol = $_SERVER["SERVER_PROTOCOL"];
			
		$this->documentRoot = $_SERVER["DOCUMENT_ROOT"];
		
		$this->applicationRoot = $_SERVER["APPLICATION_ROOT"]; // not given, must be specified on command line

		foreach ($params as $p => $v)
			$this->$p = $v;

	}
	
}

$_SERVER['APPLICATION_ROOT'] = dirname($_SERVER['DOCUMENT_ROOT']) . '/';

set_include_path(
	$_SERVER["DOCUMENT_ROOT"] . PATH_SEPARATOR
	. ($_SERVER['APPLICATION_ROOT'] . 'dep') . PATH_SEPARATOR
	. ini_get('include_path')
);

?>
