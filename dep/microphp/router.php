<?php

class MicroRouter {
	
	// TODO should probably include current_path, remaining_path etc.
	
	// TODO a renderer should probably subscribe to a route, so that the route doesn't need to know what type of renderer is chosen.
	
	function __construct(MicroPHP $application) {
		// A MicroPHP instance should probably be called a context (like scripting context), 
		// and application should be a different class.
		$this->application = $application;
	}
	
	function route() {
		$this->notFound();
	}
	
	function notFound() {
		not_found();
	}
}

/* Will route to a PHP script at a directly translated file location in document root,
		but doesn't require the .php extension, and can use a custom renderer for the script.
*/
class MicroScriptRouter extends MicroRouter {

	// TODO should probably include translated_path etc.

	function route() {
		$translatedPath = $this->application->server->applicationRoot . 'html' . $this->application->request->path;
		error_log($translatedPath);
		// TODO what is passed to the renderer? Just a filename I think, $application->renderer should be used, which has bindings for other renderers.
		// TODO route() should be used so the route is descended into.
		
		if (is_file($translatedPath))
			$this->render($translatedPath);
		else if (is_file($translatedPath . '.php'))
			$this->render($translatedPath . '.php');
		else if (is_dir($translatedPath) && is_file($translatedPath . 'index.php'))
			$this->render($translatedPath . 'index.php');
		else 
			$this->notFound();
	}

	private function render($file) {
		render($file);
	}
}

/* 

*/
class MicroActionRouter extends MicroRouter {

	function route() {
		if(!route(':module/:action'))
			$this->notFound();
	}
	
}


function not_found() {
	header ("HTTP/1.1 404 Not Found");
}

function redirect($url, $params = null) {
	header("Location: $url");
}

function rewrite() {
	// I don't think it's really possible to rewrite,
	// the difference between rewrite() and route() is that rewrite starts from and sets the beginning of the path,
	// while route() should be relative to current script.
}

/*  */
function render($scriptPath, $params = null) {
	// do output buffering
	error_log($scriptPath);
	include /* $_SERVER['DOCUMENT_ROOT'] */ $scriptPath;
}


/* checks and conditionally consumes a path against a given route with * as a wildcard for one path segment, wildcards get passed to func as params. */
function route($route, $func) {
	/* The * symbol matches one segment */
	
	$remainingAr = explode('/', trim($_SERVER['REMAINING_PATH'], '/'));
	$routeAr = explode('/', trim($route, '/'));
	$isMatch = true;
	$paramAr = array();
	//echo "$route ---\n";
	//echo "remaining " . var_dump($remainingAr);
	//echo "route " . var_dump($routeAr);
	for ($i = 0; $i < count($routeAr) && $i < count($remainingAr); $i++) {

		if ($routeAr[$i] == '*') {
			$paramAr[] = $remainingAr[$i];
		}
		
		else if ($routeAr[$i] != $remainingAr[$i]) {
			$isMatch = false;
		}

	}
	//echo "params " .var_dump($paramAr);
	
	if ($isMatch) {
		$_SERVER['CURRENT_PATH'] = rtrim($_SERVER['CURRENT_PATH'], '/') . '/' . implode('/', array_slice($remainingAr, 0, $i));
		$_SERVER['REMAINING_PATH'] = implode('/', array_slice($remainingAr, $i));
		call_user_func_array($func, $paramAr);
		//$_SERVER['CURRENT_PATH'] = ; // TODO restore original values
		return true;
	}
	else {
		return false;
	}
}

/*
 On each layer that routing is done, the routes() function should be called once with an array of all the routes on that layer,
	as opposed to calling the route() function several times, because that can lead to unexcepted behavior.
*/

function route_once($routes) {
	foreach ($routes as $route => $func) {
		if (route($route, $func))
			return true;
	}
	
	return false;
}



?>
