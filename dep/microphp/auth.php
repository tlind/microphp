<?php

class MicroAuth {
	
	private $viewer;
	
	function __construct() {
		$this->reloadSession();
	}
	
	// set the logged in viewer object
	protected function setViewer($viewer) {
		return $this->viewer = $viewer;
	}
	
	// return the viewer, regardless of logged in state
	public function getViewer($prop) {
		return (empty($prop)) ? $this->viewer : $this->viewer[$prop];
	}
	
	// return the viewer, or cause login if not logged in
	public function requireAuthenticated($prop = null) {
		if (!$this->viewer)
			$this->requireLogin();
		else
			return (!empty($prop)) ? $this->viewer[$prop] : $this->viewer;
	}
	
	public function reloadSession() {
		error_log(print_r($_COOKIE, true));
		try {
			if (isset($_COOKIE['credentials'])) {
				error_log("reloading session from credentials cookie");
				$this->viewer = $this->authenticate($_COOKIE['credentials']);
			}
		} catch (Exception $e) {
			$this->logout();
		}
	}
	
	// verify credentials, set as logged in user and start a cookie session
	public function login($credentials) {
		$this->viewer = $this->authenticate($credentials);
		// set a cookie with credentials for this session
		if ($this->viewer) {
			error_log("creating a cookie");
			$expires = time() + 60 * 60 * 24 * 30 * 2;
			setcookie("credentials[username]", $credentials['username'], $expires, '/');
			setcookie("credentials[password]", $credentials['password'], $expires, '/');
		}
	}
	
	public function logout() {
		setcookie('credentials[username]', '', time() - 3600, '/');
		setcookie('credentials[password]', '', time() - 3600, '/');
	}
	
	// check the credentials, return the object that must be set as the viewer (or null)
	public function authenticate($credentials) {
		return NULL;
	}
		
	// cause a login prompt
	public function requireLogin() {
		header('/application/entity/login.html');
	}
		
}

?>