<?php

class MicroResponse {
	
	function __construct() {
		// holds the content of all the previously rendered scripts (i.e not the currently executing script), retrieved from output buffer postProcess.
		$this->body = "";

		$response = $this;
		ob_start(function($content) use ($response) {
			// this should be the first (and therefore last) output buffer, so we can send the whole thing to the client.
			$response->body .= $content;
			return $response->body;
		});
		
	}
	
	function getBody() {
	
	}
	
	function flushBody() {
		$body = $this->body;
		$this->body = "";
		return $body;
	}
	
}

?>
