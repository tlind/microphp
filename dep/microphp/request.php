<?php

class MicroRequest {

	public function __construct($params = array()) {

		$this->method = $_SERVER["REQUEST_METHOD"];

		$this->time = $_SERVER["REQUEST_TIME"];

		$this->host = $_SERVER["HTTP_HOST"]; // the request host

		// TODO what do we do with path_info, script_filename, script_name, php_self. how do we do the nested handling (i.e path info).

		
		$this->url = $_SERVER["REQUEST_URI"]; // (includes query string. original url is given when rewriting, therefore can do routing ourselves.)

		$this->path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

		$this->query = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);

		$this->route = $this->path;

		$this->referer = @$_SERVER["HTTP_REFERER"];

		$this->params = array();

		parse_str($this->query, $this->params); // when a url rewrite is done, the request params from the url are not parsed by php.

		if (count($_POST) > 0)
			$this->params = array_merge($this->params, $_POST);
	

		$this->userAgent = $_SERVER["HTTP_USER_AGENT"];


		$this->remoteIp = $_SERVER["REMOTE_ADDR"];

		$this->remoteHost = @$_SERVER["REMOTE_HOST"];

		$this->remotePort = $_SERVER["REMOTE_PORT"];


		foreach ($params as $p => $v)
			$this->$p = $v;

	}

}

?>