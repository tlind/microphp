#!/usr/bin/env php
<?php

// must not have any .. elements in it or php-cgi will fail
$_SERVER['APPLICATION_ROOT'] = dirname(__DIR__);

$config = array(
	'var.application-root = "'.$_SERVER['APPLICATION_ROOT'].'/"'
);

file_put_contents($_SERVER['APPLICATION_ROOT'] . '/etc/app.conf', join("\n", $config));

exec("sudo killall lighttpd");
exec("sudo lighttpd -f " . $_SERVER['APPLICATION_ROOT'] . "/etc/default.conf");

?>